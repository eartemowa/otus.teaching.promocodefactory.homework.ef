﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        public string Name { get; set; }
        public List<CustomerPreference> CustomerPreference { get; set; } = new List<CustomerPreference>();
        public List<Customer> Customers { get; set; }= new List<Customer>();
        public PromoCode PromoCode { get; set; }
    }
}