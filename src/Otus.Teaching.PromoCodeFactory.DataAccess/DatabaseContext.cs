﻿using System;
using System.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;

namespace Infrastructure.EntityFramework
{
    /// <summary>
    /// Контекст
    /// </summary>
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }
        
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<CustomerPreference> CustomerPreference { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Customer>()
                .HasMany(e => e.Preferences)
                .WithMany(e => e.Customers)
                .UsingEntity<CustomerPreference>(
                 l => l.HasOne<Preference>(cp => cp.Preference)
                    .WithMany(c => c.CustomerPreference)
                    .HasForeignKey(cp => cp.PreferenceId)
                    .OnDelete(DeleteBehavior.Cascade),
                 r => r.HasOne<Customer>(cp => cp.Customer)
                    .WithMany(c => c.CustomerPreference)
                    .HasForeignKey(cp => cp.CustomerId)
                    .OnDelete(DeleteBehavior.Cascade)
            );

            modelBuilder.Entity<PromoCode>()
                .HasOne<Customer>(pc => pc.Customer)
                .WithMany(c => c.PromoCodes)
                .HasForeignKey(pc => pc.CustomerId);

            modelBuilder.Entity<Role>()
                .HasOne<Employee>(r => r.Employee)
                .WithOne(e => e.Role)
                .HasForeignKey<Employee>(e => e.RoleId);

            modelBuilder.Entity<Preference>()
                .HasOne<PromoCode>(p => p.PromoCode)
                .WithOne(pc => pc.Preference)
                .HasForeignKey<PromoCode>(pc => pc.PreferenceId);

            modelBuilder.Entity<Customer>().Property(c => c.FirstName).HasMaxLength(100);
            modelBuilder.Entity<Customer>().Property(c => c.LastName).HasMaxLength(100);
            modelBuilder.Entity<Customer>().Property(c => c.Email).HasMaxLength(100);

            modelBuilder.Entity<Preference>().Property(p => p.Name).HasMaxLength(100);

            modelBuilder.Entity<Employee>().Property(c => c.FirstName).HasMaxLength(100);
            modelBuilder.Entity<Employee>().Property(c => c.LastName).HasMaxLength(100);
            modelBuilder.Entity<Employee>().Property(c => c.Email).HasMaxLength(100);

            modelBuilder.Entity<Role>().Property(r => r.Name).HasMaxLength(100);
            modelBuilder.Entity<Role>().Property(r => r.Description).HasMaxLength(100);

            modelBuilder.Entity<PromoCode>().Property(pc => pc.Code).HasMaxLength(100);
            modelBuilder.Entity<PromoCode>().Property(pc => pc.ServiceInfo).HasMaxLength(100);
            modelBuilder.Entity<PromoCode>().Property(pc => pc.PartnerName).HasMaxLength(100);

            DataSeed.SeedData(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);   
        }
    }
}