﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataSeed
    {
        public static void SeedData(ModelBuilder builder)
        {
            foreach (Customer customer in FakeDataFactory.Customers)
            {
                builder.Entity<Customer>().HasData(customer);
            }
            foreach (Role roles in FakeDataFactory.Roles)
            {
                builder.Entity<Role>().HasData(roles);
            }
            foreach (Employee employee in FakeDataFactory.Employees)
            {
                builder.Entity<Employee>(e =>
                {
                    e.HasData(new Employee
                    {
                        Id = employee.Id,
                        Email = employee.Email,
                        FirstName = employee.FirstName,
                        LastName = employee.LastName,
                        RoleId = employee.Role.Id,
                        AppliedPromocodesCount = employee.AppliedPromocodesCount,
                    });
                });
            }
            foreach (Preference preference in FakeDataFactory.Preferences)
            {
                builder.Entity<Preference>().HasData(preference);
            }
            foreach (CustomerPreference customerPreference in FakeDataFactory.CustomerPreference)
            {
                builder.Entity<CustomerPreference>().HasData(customerPreference);
            }
        }
    }
}
