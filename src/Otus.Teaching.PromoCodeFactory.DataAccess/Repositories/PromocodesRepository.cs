﻿using Infrastructure.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
//using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class PromocodesRepository : EfRepository<PromoCode, DatabaseContext>, IPromoCodeRepository
    {
        public PromocodesRepository(DatabaseContext context) : base(context)
        {

        }

        public async Task<PromoCode> AddToCustomersWithPreferenceAsync(PromoCode entity) 
        {
            Preference preference = Context.Preferences.First(p => p.Name == entity.Preference.Name);
            if (preference != null)
            {
                Guid preferenceId = preference.Id;
                entity.PreferenceId = preferenceId;
                CustomerPreference customerPreference = Context.CustomerPreference.First(c => c.PreferenceId == preferenceId);
                if (customerPreference != null)
                {
                    entity.CustomerId = customerPreference.CustomerId;
                    await Context.PromoCodes.AddAsync(entity);
                    await Context.SaveChangesAsync();
                }
            }
            return entity;
        }
    }
}
