﻿using Infrastructure.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
//using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerRepository : EfRepository<Customer, DatabaseContext>, ICustomerRepository
    {
        public CustomerRepository(DatabaseContext context) : base(context)
        {

        }

        public async Task<Customer> GetCustomerById(Guid id)
        {
            return await Context.Customers.Include(p =>p.Preferences).FirstAsync(c => c.Id == id);             
        }
    }
}
