﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PreferenceResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public PromoCode PromoCode { get; set; }
    }
}