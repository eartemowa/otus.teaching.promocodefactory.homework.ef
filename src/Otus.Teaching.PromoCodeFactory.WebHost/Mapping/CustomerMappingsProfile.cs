﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapping
{
    public class CustomerMappingsProfile : Profile
    {
        public CustomerMappingsProfile() 
        {
            CreateMap<Customer, CustomerShortResponse>();
            CreateMap<Customer, CustomerResponse>();
            CreateMap<Preference, PreferenceResponse>();
            CreateMap<Customer, CreateOrEditCustomerRequest>();
            CreateMap<CreateOrEditCustomerRequest, Customer>()
                .ForMember(entity => entity.CustomerPreference, opt => opt.MapFrom(x => x.PreferenceIds.Select(id => new CustomerPreference { PreferenceId = id })));
            CreateMap<PromoCode, PromoCodeShortResponse>();
            CreateMap<GivePromoCodeRequest, PromoCode>()
                .ForMember(entity => entity.Preference, opt => opt.MapFrom(x => new Preference { Name = x.Preference }))
                .ForMember(entity => entity.Code, opt => opt.MapFrom(x => x.PromoCode));
            CreateMap<PromoCode, GivePromoCodeRequest>()
                .ForMember(opt => opt.PromoCode, entity => entity.MapFrom(x => x.Code));
        }
    }
}
